﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace ResetAspNetMembershipUser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Reset AspNet membership User Password");

            ConsoleKeyInfo action = new ConsoleKeyInfo();

            do
            {
                try
                {
                    action = GetAction();
                    Console.WriteLine("");

                    switch(action.KeyChar)
                    {
                        case '1':
                            ListUsers();
                            break;

                        case '2':
                            ResetUser();
                            break;

                        case '0':
                            //do nothing, we are going to exit
                            break;
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            } while (action.KeyChar != '0');


        }

        static ConsoleKeyInfo GetAction()
        {
            Console.WriteLine("Select Action:");
            Console.WriteLine("1) List Users");
            Console.WriteLine("2) Reset Password");
            Console.WriteLine("0) Done");

            return Console.ReadKey();
        }

        static void ResetUser()
        {
            Console.Write("Username:");
            string username = Console.ReadLine();

            Console.Write("New Password:");
            string newPassword = Console.ReadLine() ?? string.Empty;

            MembershipUser membershipUser = Membership.GetUser(username);

            if(membershipUser == null)
            {
                Console.WriteLine("Username not found: " + username);
                return;
            }

            string generatedPassword = membershipUser.ResetPassword();
            membershipUser.ChangePassword(generatedPassword, newPassword);


        }

        static void ListUsers()
        {
            Console.WriteLine("Getting User(s)");

            MembershipUserCollection users = Membership.GetAllUsers();

            Console.WriteLine(users.Count + " User(s)");

            int userCtr = 0;

            

            foreach (MembershipUser user in users)
            {
                userCtr++;

                Console.WriteLine(user.UserName);

                //every 10 ask if the user wants to continue
                if ((userCtr % 10) == 0)
                {
                    Console.WriteLine("Continue? Y/N");
                    var keepGoing = Console.ReadKey();

                    if(keepGoing.Key != ConsoleKey.Y)
                        break;
                }
            }
        }
    }
}
